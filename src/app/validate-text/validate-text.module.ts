import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ValidateTextPageRoutingModule } from './validate-text-routing.module';

import { ValidateTextPage } from './validate-text.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ValidateTextPageRoutingModule
  ],
  declarations: [ValidateTextPage]
})
export class ValidateTextPageModule {}
