import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ValidateTextPage } from './validate-text.page';

describe('ValidateTextPage', () => {
  let component: ValidateTextPage;
  let fixture: ComponentFixture<ValidateTextPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ValidateTextPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ValidateTextPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
