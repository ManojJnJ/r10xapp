import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ValidateTextPage } from './validate-text.page';

const routes: Routes = [
  {
    path: '',
    component: ValidateTextPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ValidateTextPageRoutingModule {}
